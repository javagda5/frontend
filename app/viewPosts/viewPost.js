'use strict';

angular.module('myApp.viewPost', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewpost', {
            templateUrl: 'viewPost/viewPost.html',
            controller: 'ViewPostCtrl'
        });
    }])

    .controller('ViewPostCtrl', ['$http', '$rootScope', 'AuthService', function ($http, $rootScope, AuthService) {
        var URL = 'http://localhost:8080';
        var self = this;

        // AuthService.loggedInUser.id;

    }]);