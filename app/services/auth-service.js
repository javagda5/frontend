'use strict';

angular.module('myApp.authService', ['ngRoute'])
    .service('AuthService', [function () {
        this.logged_in_id = '';
        this.loggedInUser = {
            'id': '',
            'username': ''
        };
    }]);